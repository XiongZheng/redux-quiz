import {combineReducers} from "redux";
import createReducer from "../create/reducers/createReducer";
import noteReducer from "../note/reducers/noteReducer";
import detailsReducer from "../details/reducers/detailsReducer";
import marksReducer from "./marksReducer";

const reducers = combineReducers({
  createReducer,
  noteReducer,
  detailsReducer,
  marksReducer
});
export default reducers;