const initState = {
  currentId: 1,
  result:false
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'CHANGE_CURRENT_ID':
      return {
        ...state,
        currentId: action.currentId
      };
    case 'DELETE_MARKDOWN_BY_ID':
      return {
        ...state,
        result:action.result
      };
    default:
      return state
  }
};
