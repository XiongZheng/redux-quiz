import React, { Fragment } from 'react'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { deleteMarkDown } from "../actions/detailsActions";


export class RightBody extends React.Component {

  handlerBackHome() {
    window.location.href = '/';
  }

  deleteMarkDown(event) {
    this.props.deleteMarkDown(event.target.id);
    window.location.href = '/';
  }

  render() {
    const currentId = this.props.detailsReducer.currentId;
    const marks = this.props.marksReducer.markDowns;
    return (
      <Fragment>
        {
          marks.map((value) => {
            if (Number.parseInt(value.id) === Number.parseInt(currentId)) {
              return (
                <div className={'right-div'}>
                  <p>{value.title}</p>
                  <p>{value.description}</p>
                  <button onClick={this.deleteMarkDown.bind(this)} id={value.id} className={'delete-button'}>删除</button>
                  <button onClick={this.handlerBackHome.bind(this)} className={'cancel-button'}>取消</button>
                </div>
              )
            }
          })
        }
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  detailsReducer: state.detailsReducer,
  marksReducer: state.marksReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    deleteMarkDown
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RightBody);
