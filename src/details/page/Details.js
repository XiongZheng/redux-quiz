import React,{Component} from 'react'
import './details.less'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LeftMenu from "../components/LeftMenu";
import { changeCurrentId } from "../actions/detailsActions";
import  RightBody  from "../components/RightBody";

export class Details extends Component {

  componentDidMount() {
  }

  constructor(props) {
    super(props);
    this.props.changeCurrentId(this.props.match.params.id);
  }

  render() {
    return (
      <div className={'details-body'}>
        <LeftMenu/>
        <RightBody/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  detailsReducer: state.detailsReducer,
  marksReducer: state.marksReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    changeCurrentId
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Details);

