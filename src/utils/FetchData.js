export const fetchDataByPost = (url, data) => {
  const bodyData = JSON.stringify(data);
  return fetch(url, {
    method: 'POST',
    headers: {
      'content-type': 'application/json;charset=UTF-8'
    },
    body: bodyData
  }).then(response => {
    return response.ok;
  })
};

export const fetchDataByGet = (url) => {
  return fetch(url, {
    method: 'GET',
  }).then(response => response.json())
};
export const fetchDataByDelete = (url) => {
  return fetch(url, {
    method: 'DELETE',
  }).then(response => {
    return response.ok;
  })
};
