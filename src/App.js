import React,{Component} from 'react';
import './App.less';
import {Route, Switch} from "react-router";
import {BrowserRouter as Router} from "react-router-dom";
import Note from './note/page/Note'
import Create from "./create/page/Create";
import Details from "./details/page/Details";
import homeImg from './resource/home.png'

class App extends Component {
  render() {
    return (
        <Router>
          <div className='header-div' >
            <img src={homeImg} alt="home"/>
            <span>NOTES</span>
          </div>
          <Switch>
            <Route path="/" exact component={Note}/>
            <Route path="/notes/create" component={Create}/>
            <Route path="/notes/:id" component={Details}/>
          </Switch>
        </Router>
    )
  }
}

export default App;