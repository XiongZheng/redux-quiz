import React, {Component} from 'react'
import './create.less'
import Button from "../components/Button";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {changeDescription, changeTitle, commitMarkDown,changeCommitResult} from "../actions/createActions";

export class Create extends Component {
  handlerChangeTitle(event) {
    this.props.changeTitle(event.target.value)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.createReducer.commitResult) {
      this.props.changeCommitResult(false);
      window.location.href = '/';
    }
  }

  handlerChangeDescription(event) {
    this.props.changeDescription(event.target.value)
  }

  render() {
    return (
      <div className='create-note'>
        <h2>创建笔记</h2>
        <hr/>
        <label>标题</label>
        <br/>
        <input onChange={this.handlerChangeTitle.bind(this)} className='create-title' type="text"/>
        <label>正文</label>
        <textarea id="create-button" cols="100 " rows="10" onChange={this.handlerChangeDescription.bind(this)}/>
        <Button createReducer={this.props.createReducer} commitMarkDown={this.props.commitMarkDown} isDisable={this.props.createReducer.commitDisable} buttonClass={'submit-button'} name={'提交'}/>
        <Button isDisable={false} buttonClass={'cancel-button'} name={'取消'}/>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  createReducer: state.createReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    commitMarkDown,
    changeTitle,
    changeDescription,
    changeCommitResult
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Create);
