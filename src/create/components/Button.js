import React,{Component} from 'react'

export default class Button extends Component {

  handlerClick() {
    if (this.props.name === '提交') {
      this.props.commitMarkDown(this.props.createReducer);
    }
    if (this.props.name === '取消') {
      window.location.href = '/';
    }
  }

  render() {
    return (
      <button onClick={this.handlerClick.bind(this)} className={this.props.buttonClass} disabled={this.props.isDisable}>{this.props.name}</button>
    )
  }
}
