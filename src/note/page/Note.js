import React, {Component} from 'react'
import './note.less'
import {bindActionCreators} from "redux";
import {getAll} from "../actions/NoteActions";
import {connect} from "react-redux";
import Show from "../components/Show";

export class Note extends Component {

  componentDidMount() {
    this.props.getAll();
  }

  render() {

    return (
      <div className='note-body'>
        <Show marks={this.props.noteReducer.markDowns}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  noteReducer: state.noteReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getAll
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Note);

