import React from 'react'
import {Link} from "react-router-dom";
import add from '../../resource/add.png'

class Show extends React.Component {

  render() {
    const mark = this.props.marks;
    return (
      <div className={'show-body'}>
        {mark.map(((value, index) => {
          return (
            <div className='home-item-div'>
              <Link key={'div' + index} to={'/notes/' + value.id}>
                <span>{value.title}</span>
              </Link>
              <br/>
            </div>
          )
        }))
        }
        <div className='home-item-div'>
          <Link to={'/notes/create'}><img src={add} alt=""/></Link>
        </div>
      </div>
    );
  }
}

export default Show;
